# Objetivo

Este é um projeto de estudo e tem como objetivo implementar uma aplicação crud simples em Vue.js com o backend em PHP, fazendo autenticação com JWT.

## Build frontend

``` bash
# instalar dependências
npm install

# serve os arquivos com hot reload em localhost:8080
npm run hot

# gera os arquivos js/css
npm run build
```

## Build backend

``` bash
# instalar dependências
composer install

# inicia o servidor de desenvolvimento do PHP
php -S localhost:9000 -t public
```
