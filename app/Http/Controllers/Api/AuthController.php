<?php

namespace App\Http\Controllers\Api;

class AuthController
{
    public function authenticate($request, $response)
    {
        $body = $request->getParsedBody();
        $username = $body['username'] ?? '';
        $password = $body['password'] ?? '';

        if ($username === 'admin' && $password === '123') {
            return $response->withJson([
                'message' => 'ok'
            ]);
        }
        return $response->withJson([
            'message' => 'Authentication failed'
        ], 401);
    }
}
