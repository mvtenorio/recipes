<?php

namespace App\Http\Controllers;

class PagesController
{
    protected $view;

    public function __construct(\Slim\Views\Twig $view) {
        $this->view = $view;
    }

    public function showLogin($request, $response)
    {
        return $this->view->render($response, 'pages/login.html.twig');
    }

    public function showHome($request, $response)
    {
        return $this->view->render($response, 'pages/home.html.twig');
    }
}
