<?php

namespace App\Http\Middleware;

class GuestMiddleware
{
    /**
     * Redirects if the user is already logged in
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        if (isset($_SESSION['user'])) {
            return $response
                ->withStatus(302)
                ->withHeader('Location', '/');
        }
        return $next($request, $response);
    }
}
