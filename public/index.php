<?php

require '../vendor/autoload.php';

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\PagesController;

// Define settings
$config = [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ]
];

// Create App
$app = new \Slim\App($config);

// Get container
$container = $app->getContainer();

// Register component on container
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig('../resources/templates');

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');

    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));

    return $view;
};

$container['App\Http\Controllers\PagesController'] = function($c) {
    $view = $c->get("view");
    return new PagesController($view);
};

// Define routes

// pages
$app->get('/login', PagesController::class.':showLogin');
$app->get('/', PagesController::class.':showHome');

// api
$app->group('/api', function() {
    $this->post('/auth', AuthController::class.':authenticate');
});

$app->run();
