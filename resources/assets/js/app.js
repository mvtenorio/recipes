import Vue from 'vue'
import Home from './pages/Home.vue'

new Vue({
  el: '#app',
  render: h => h(Home)
})
