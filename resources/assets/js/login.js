import Vue from 'vue'
import Login from './pages/Login.vue'

new Vue({
  el: '#app',
  render: h => h(Login)
})
